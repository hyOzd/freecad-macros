#!/usr/bin/python
#
# This is a cadquery script for generating QFP models in X3D format.
#
# Dimensions are from Jedec MS-026D document.

import cadquery as cq
from Helpers import show
from math import tan, radians, sqrt
from collections import namedtuple
from export_x3d import exportX3D, Mesh
import sys, os

case_color = (0.1, 0.1, 0.1)
pins_color = (0.9, 0.9, 0.9)

# common dimensions from MS-026D
the = 12.0      # body angle in degrees
tb_s = 0.15     # top part of body is that much smaller

c = 0.1         # pin thickness, body center part height
R1 = 0.1        # pin upper corner, inner radius
R2 = 0.1        # pin lower corner, inner radius
S = 0.2         # pin top flat part length (excluding corner arc)

# other common dimensions
fp_r = 0.5      # first pin indicator radius
fp_d = 0.2      # first pin indicator distance from edge
fp_z = 0.1      # first pin indicator depth
ef = 0.05       # fillet of edges
max_cc1 = 1     # maximum size for 1st pin corner chamfer

Params = namedtuple("Params", [
    'D',    # overall width
    'E',    # overall length
    'D1',   # body width
    'E1',   # body length
    'A1',   # body-board seperation
    'A2',   # body height

    'b',    # pin width
    'e',    # pin (center-to-center) distance

    'npx',  # number of pins along X axis (width)
    'npy',  # number of pins along y axis (length)
    'epad'  # exposed pad, None or the dimensions as tuple: (width, length)
])

all_params = {
    'AKA': Params( # 4x4, pitch 0.65 20pin 1mm height
        D = 6.0,   # overall width
        E = 6.0,   # overall length
        D1 = 4.0,  # body width
        E1 = 4.0,  # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.32,  # pin width
        e = 0.65,  # pin (center-to-center) distance
        npx = 5,   # number of pins along X axis (width)
        npy = 5,   # number of pins along y axis (length)
        epad = None),
    
    'ABD': Params( # 7x7, 0.4 pitch, 64 pins, 1mm height
        D = 9.0,   # overall width
        E = 9.0,   # overall length
        D1 = 7.0, # body width
        E1 = 7.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.18,  # pin width
        e = 0.4,   # pin (center-to-center) distance
        npx = 16,  # number of pins along X axis (width)
        npy = 16,  # number of pins along y axis (length)
        epad = None),

    'AFB': Params( # 20x20, 0.5 pitch, 144pins, 1mm height
        D = 22.0,   # overall width
        E = 22.0,   # overall length
        D1 = 20.0, # body width
        E1 = 20.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.22,  # pin width
        e = 0.5,   # pin (center-to-center) distance
        npx = 36,  # number of pins along X axis (width)
        npy = 36,  # number of pins along y axis (length)
        epad = None),

    'ACB': Params( # 10x10, 0.8 pitch, 44 pins, 1mm height
        D = 12.0,   # overall width
        E = 12.0,   # overall length
        D1 = 10.0, # body width
        E1 = 10.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.37,  # pin width
        e = 0.8,   # pin (center-to-center) distance
        npx = 11,  # number of pins along X axis (width)
        npy = 11,  # number of pins along y axis (length)
        epad = None),

    'ACC': Params( # 10x10, 0.65 pitch, 52 pins, 1mm height
        D = 12.0,   # overall width
        E = 12.0,   # overall length
        D1 = 10.0, # body width
        E1 = 10.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.32,  # pin width
        e = 0.65,  # pin (center-to-center) distance
        npx = 13,  # number of pins along X axis (width)
        npy = 13,  # number of pins along y axis (length)
        epad = None),

    'ACE': Params( # 10x10, 0.4 pitch, 80 pins, 1mm height
        D = 12.0,   # overall width
        E = 12.0,   # overall length
        D1 = 10.0, # body width
        E1 = 10.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.18,  # pin width
        e = 0.4,  # pin (center-to-center) distance
        npx = 20,  # number of pins along X axis (width)
        npy = 20,  # number of pins along y axis (length)
        epad = None),

    'ADC': Params( # 12x12, 0.65 pitch, 64 pins, 1mm height
        D = 14.0,   # overall width
        E = 14.0,   # overall length
        D1 = 12.0, # body width
        E1 = 12.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.32,  # pin width
        e = 0.65,  # pin (center-to-center) distance
        npx = 13,  # number of pins along X axis (width)
        npy = 13,  # number of pins along y axis (length)
        epad = None),

    'ADD': Params( # 12x12, 0.5 pitch, 80 pins, 1mm height
        D = 14.0,   # overall width
        E = 14.0,   # overall length
        D1 = 12.0, # body width
        E1 = 12.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.18,  # pin width
        e = 0.5,  # pin (center-to-center) distance
        npx = 20,  # number of pins along X axis (width)
        npy = 20,  # number of pins along y axis (length)
        epad = None),

    'AEC': Params( # 14x14, 0.65 pitch, 80 pins, 1mm height
        D = 16.0,   # overall width
        E = 16.0,   # overall length
        D1 = 14.0, # body width
        E1 = 14.0, # body length
        A1 = 0.1,  # body-board seperation
        A2 = 1.0,  # body height
        b = 0.32,  # pin width
        e = 0.65,  # pin (center-to-center) distance
        npx = 20,  # number of pins along X axis (width)
        npy = 20,  # number of pins along y axis (length)
        epad = None),
}

def almost_equal(x, y, t=0.0001):
    return abs(x - y) < t

def make_qfp(params):

    D   = params.D
    E   = params.E
    D1  = params.D1
    E1  = params.E1
    A1  = params.A1
    A2  = params.A2
    b   = params.b
    e   = params.e
    npx = params.npx
    npy = params.npy

    if params.epad:
        D2 = params.epad[0]
        E2 = params.epad[1]

    if not almost_equal(D1-D, E1-E):
        raise Exception("This script assumes D1-D==E1-E !")

    # calculated dimensions for body
    A = A1 + A2
    A2_t = (A2-c)/2 # body top part height
    A2_b = A2_t     # body bottom part height
    D1_b = D1-2*tan(radians(the))*A2_b # bottom width
    E1_b = E1-2*tan(radians(the))*A2_b # bottom length
    D1_t1 = D1-tb_s # top part bottom width
    E1_t1 = E1-tb_s # top part bottom length
    D1_t2 = D1_t1-2*tan(radians(the))*A2_t # top part upper width
    E1_t2 = E1_t1-2*tan(radians(the))*A2_t # top part upper length

    cc1 = 0.45 # chamfer of the 1st pin corner
    cc = 0.25  # chamfer of the other corners

    # calculate chamfers
    totpinwidthx = (npx-1)*e+b # total width of all pins on the X side
    totpinwidthy = (npy-1)*e+b # total width of all pins on the Y side

    cc1 = min((D1-totpinwidthx)/2., (E1-totpinwidthy)/2.) - 0.5*tb_s
    cc1 = min(cc1, max_cc1)
    cc = cc1/2.

    def crect(wp, rw, rh, cv1, cv):
        """
        Creates a rectangle with chamfered corners.
        wp: workplane object
        rw: rectangle width
        rh: rectangle height
        cv1: chamfer value for 1st corner (lower left)
        cv: chamfer value for other corners
        """
        points = [
            (-rw/2., -rh/2.+cv1),
            (-rw/2., rh/2.-cv),
            (-rw/2.+cv, rh/2.),
            (rw/2.-cv, rh/2.),
            (rw/2., rh/2.-cv),
            (rw/2., -rh/2.+cv),
            (rw/2.-cv, -rh/2.),
            (-rw/2.+cv1, -rh/2.),
            (-rw/2., -rh/2.+cv1)
        ]
        return wp.polyline(points)

    case = cq.Workplane(cq.Plane.XY()).workplane(offset=A1)
    case = crect(case, D1_b, E1_b, cc1-(D1-D1_b)/4., cc-(D1-D1_b)/4.)  # bottom edges
    case = case.pushPoints([(0,0)]).workplane(offset=A2_b)
    case = crect(case, D1, E1, cc1, cc)     # center (lower) outer edges
    case = case.pushPoints([(0,0)]).workplane(offset=c)
    case = crect(case, D1,E1,cc1, cc)       # center (upper) outer edges
    case = crect(case, D1_t1,E1_t1, cc1-(D1-D1_t1)/4., cc-(D1-D1_t1)/4.) # center (upper) inner edges
    case = case.pushPoints([(0,0)]).workplane(offset=A2_t)
    cc1_t = cc1-(D1-D1_t2)/4. # this one is defined because we use it later
    case = crect(case, D1_t2,E1_t2, cc1_t, cc-(D1-D1_t2)/4.) # top edges
    case = case.loft(ruled=True).faces(">Z").fillet(ef)

    # first pin indicator is created with a spherical pocket
    sphere_r = (fp_r*fp_r/2 + fp_z*fp_z) / (2*fp_z)
    sphere_z = A + sphere_r * 2 - fp_z - sphere_r
    sphere_x = -D1_t2/2.+cc1_t/2.+(fp_d+fp_r)/sqrt(2)
    sphere_y = -E1_t2/2.+cc1_t/2.+(fp_d+fp_r)/sqrt(2)
    sphere = cq.Workplane("XY", (sphere_x, sphere_y, sphere_z)). \
             sphere(sphere_r)
    case = case.cut(sphere)

    # calculated dimensions for pin
    R1_o = R1+c # pin upper corner, outer radius
    R2_o = R2+c # pin lower corner, outer radius
    L = (D-D1)/2.-R1-R2-S

    # Create a pin object at the center of top side.
    bpin = cq.Workplane("YZ", (0,E1/2,0)). \
        moveTo(-tb_s, A1+A2_b). \
        line(S+tb_s, 0). \
        threePointArc((S+R1/sqrt(2), A1+A2_b-R1*(1-1/sqrt(2))),
                      (S+R1, A1+A2_b-R1)). \
        line(0, -(A1+A2_b-R1-R2_o)). \
        threePointArc((S+R1+R2_o*(1-1/sqrt(2)), R2_o*(1-1/sqrt(2))),
                      (S+R1+R2_o, 0)). \
        line(L-R2_o, 0). \
        line(0, c). \
        line(-(L-R2_o), 0). \
        threePointArc((S+R1+R2_o-R2/sqrt(2), c+R2*(1-1/sqrt(2))),
                      (S+R1+R2_o-R1, c+R2)). \
        lineTo(S+R1+c, A1+A2_b-R1). \
        threePointArc((S+R1_o/sqrt(2), A1+A2_b+c-R1_o*(1-1/sqrt(2))),
                      (S, A1+A2_b+c)). \
        line(-S-tb_s, 0).close().extrude(b).translate((-b/2,0,0))

    pins = []
    # create top, bottom side pins
    first_pos = -(npx-1)*e/2
    for i in range(npx):
        pin = bpin.translate((first_pos+i*e, 0, 0))
        pins.append(pin)
        pin = bpin.translate((first_pos+i*e, 0, 0)).\
              rotate((0,0,0), (0,0,1), 180)
        pins.append(pin)

    # create right, left side pins
    first_pos = -(npy-1)*e/2
    for i in range(npy):
        pin = bpin.translate((first_pos+i*e, (D1-E1)/2, 0)).\
              rotate((0,0,0), (0,0,1), 90)
        pins.append(pin)
        pin = bpin.translate((first_pos+i*e, (D1-E1)/2, 0)).\
              rotate((0,0,0), (0,0,1), 270)
        pins.append(pin)

    # create exposed thermal pad if requested
    if params.epad:
        pins.append(cq.Workplane("XY").box(D2, E2, A1).translate((0,0,A1/2)))

    # merge all pins to a single object
    merged_pins = pins[0]
    for p in pins[1:]:
        merged_pins = merged_pins.union(p)
    pins = merged_pins

    # extract pins from case
    case = case.cut(pins)

    return (case, pins)

def shapeToMesh(shape, color):
    mesh_data = shape.tessellate(1)
    return Mesh(points = mesh_data[0],
                faces = mesh_data[1],
                color = color)

def make_one(variant, filename):
    """Generates an X3D file for given variant. Variants parameters must
    be entered into `all_params` data structure.
    """
    print("Generating QFP package model for %s variant..." % variant)
    case, pins = make_qfp(all_params[variant])
    exportX3D([shapeToMesh(case.toFreecad(), case_color),
               shapeToMesh(pins.toFreecad(), pins_color)],
              filename)
    print("Done generating QFP %s variant." % variant)

def run():
    # get variant names from command line
    if len(sys.argv) < 2:
        print("No variant name is given!")
        return

    if sys.argv[1] == "all":
        variants = all_params.keys()
    else:
        variants = sys.argv[1:]

    outdir = os.path.abspath("./generated_qfp/")
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    for variant in variants:
        if not variant in all_params:
            print("Parameters for %s doesn't exist in 'all_params', skipping." % variant)
            continue
        make_one(variant, outdir + ("/qfp_%s.x3d" % variant))

if __name__ == "__main__":
    run()

# when run from freecad-cadquery
if __name__ == "temp.module":
    case, pins = make_qfp(all_params['AKA'])

    show(case, (80, 80, 80, 0))
    show(pins)
