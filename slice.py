import FreeCAD as App
import FreeCADGui as Gui
from FreeCAD import Base
import Part, Draft

from operator import attrgetter

def findPlane(obj):
    """Finds the plane of a 'sheet like' object. Returns the normal."""
    # First, find the normal (orientation) of the object For this we
    # will first find its main flat surface. We are assuming objects
    # biggest (by area) flat face will be its cut path.
    faces = obj.Shape.Faces

    # filter planar faces
    def isPlanar(face):
        return face.Surface.__class__.__name__ == 'GeomPlane'

    flat_faces = filter(isPlanar, faces)

    if not len(flat_faces):
        raise Exception("Object doesn't have any flat faces!")

    # get the biggest flat face
    big_face = sorted(flat_faces, key=attrgetter('Area'), reverse=True)[0]

    return big_face.Surface.Axis

def run():
    objects = Gui.Selection.getSelection()
    if not len(objects):
        raise Exception("No objects selected")

    for obj in objects:
        print("Slicing: %s" % obj.Label)
        normal = findPlane(obj)
        Draft.makeShape2DView(obj, normal)

if __name__ == "__main__":
    run()
