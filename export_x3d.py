#
# This is an experimental FreeCAD macro that will export current scene
# as X3D file. Only the visible objects that can be converted to mesh
# will be exported with their color.
#

import FreeCAD
import FreeCADGui
import xml.etree.ElementTree as et
from PySide.QtGui import QFileDialog
import os
from collections import namedtuple

# points: [Vector, Vector, ...]
# faces: [(pi, pi, pi), ], pi: point index
# color: (Red, Green, Blue), values range from 0 to 1.0
Mesh = namedtuple('Mesh', ['points', 'faces', 'color'])

def getShapeNode(vertices, faces, diffuseColor=None):
    """Returns a <Shape> node for given mesh data.
    vertices: list of vertice coordinates as `Vector` type
    faces: list of tuple of vertice indexes ex: (1, 2, 3)
    diffuseColor: tuple in the form of (R, G, B)"""

    shapeNode = et.Element('Shape')
    faceNode = et.SubElement(shapeNode, 'IndexedFaceSet')
    faceNode.set('coordIndex', ' '.join(["%d %d %d -1" % face for face in faces]))
    coordinateNode = et.SubElement(faceNode, 'Coordinate')
    coordinateNode.set('point',
        ' '.join(["%f %f %f" % (p.x, p.y, p.z) for p in vertices]))

    if diffuseColor:
        appearanceNode = et.SubElement(shapeNode, 'Appearance')
        materialNode = et.SubElement(appearanceNode, 'Material')
        materialNode.set('diffuseColor', "%f %f %f" % diffuseColor)

    return shapeNode

def exportX3D(objects, filepath):
    """Export given list of Mesh objects to a X3D file."""

    fileNode = et.Element('X3D')
    fileNode.set('profile', 'Interchange')
    fileNode.set('version', '3.3')
    sceneNode = et.SubElement(fileNode, 'Scene')

    for o in objects:
        shapeNode = getShapeNode(o.points, o.faces, o.color)
        sceneNode.append(shapeNode)

    with open(filepath, "wr") as f:
        f.write(et.tostring(fileNode))

def getDocumentDir(doc):
    """Returns directory for given document. `None` if the file is not
    saved yet."""
    if doc.FileName:
        return os.path.dirname(doc.FileName)
    else:
        return None

def objectToMesh(obj, tessellation = 1.0):
    """Returns a Mesh object from given FreeCAD object. Returns None if
    object cannot be converted to Mesh."""
    if hasattr(obj, 'Shape'):
        mesh = obj.Shape.tessellate(tessellation)
        if (not mesh[0]) or (not mesh[1]):
            # some objects (such as Part:Circle) generate empty mesh
            return None
        else:
            return Mesh(points = mesh[0],
                        faces = mesh[1],
                        color = obj.ViewObject.ShapeColor[0:3])
    else:
        return None

def run():
    doc = FreeCAD.activeDocument()

    objects = []

    for o in doc.Objects:
        if o.ViewObject.Visibility:
            mesh = objectToMesh(o)
            if mesh:
                objects.append(mesh)

    if objects:
        savefile = QFileDialog.getSaveFileName(
            parent = FreeCADGui.getMainWindow(),
            caption = "Export X3D file",
            dir = getDocumentDir(doc))[0]
        exportX3D(objects, savefile)
    else:
        raise Exception("There is nothing to export!")

if __name__ == "__main__":
    run()
