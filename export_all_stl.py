#
# Export All STL
#
# This is a small FreeCAD script to export all visible parts in STL
# mesh format. Files will be named as "documentname_partlabel.stl".
#

import FreeCAD
import os.path

doc = FreeCAD.activeDocument()
base_filename = os.path.splitext(doc.FileName)[0]

for obj in doc.Objects:
    if obj.ViewObject.Visibility:
        filename = base_filename + "_" + obj.Label + ".stl"
        obj.Shape.exportStl(filename)
