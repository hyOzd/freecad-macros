#!/usr/bin/python
#
# This is an exporter script for exporting cadquery objects as STEP
# models. It uses FreeCADs gui library. Note that this library
# initalizes FreeCAD's mainwindow thus enables some GUI
# functionality. This may have some unknown (yet) side effects.
#

import os
import FreeCAD, FreeCADGui
import cadquery as cq

def makeFCObject(doc, cqobject, color=None):
    """Creates an Object in document tree.
    `doc` : FreeCAD document object
    `cqobject` : cadquery object
    `color` : color RGB tuple
    """
    o = doc.addObject("Part::Feature", "Box")
    o.Shape = cqobject.toFreecad()
    if color: o.ViewObject.ShapeColor = color
    return o


def exportx3d(objects, filename):
    from export_x3d import exportX3D, objectToMesh, Mesh

    def shapeToMesh(shape, color):
        mesh_data = shape.tessellate(1)
        return Mesh(points = mesh_data[0],
                    faces = mesh_data[1],
                    color = color)

    meshes = []
    for o in objects:
        meshes.append(shapeToMesh(o[0].toFreecad(), o[1]))

    exportX3D(meshes, filename)

def export(ftype, objects, filename, fuse=False):
    """ Exports given list of objects using FreeCAD.

    `ftype` : one of "STEP", "VRML", "FREECAD", "X3D"
    `objects` : list of tuples (cqobject, color)
    `filename` : name of the file, extension is important
    `fuse` : fuse objects together before export (preserves color)

    cqobject : cadquery object that can be converted to shape with
    `toFreecad()` method.
    color : is a tuple of (R, G, B), values range from 0.0 to 1.0

    color is optional, will be omitted if `None`.

    X3D exporter doesn't support `fuse` parameter.
    """
    if ftype == "X3D":
        if fuse: print("X3D exporter can't do fuse, ignoring.")
        exportx3d(objects, filename)
        return

    # continue for other exporters (VRML, FREECAD, STEP)

    # init FreeCADGui
    try:
        import ImportGui
    except ImportError:
        FreeCADGui.showMainWindow()
        FreeCADGui.getMainWindow().hide() # prevent splash of main window
        import ImportGui # must be after `showMainWindow`

    # make sure RefineShape=False
    pg = FreeCAD.ParamGet("User parameter:BaseApp/Preferences/Mod/Part/Boolean")
    usersRSOption = pg.GetBool("RefineModel") # will be restored, we promise
    pg.SetBool("RefineModel", False)

    # create a FreeCAD document
    doc = FreeCAD.newDocument()

    # create objects
    fcobjects = [makeFCObject(doc, co[0], co[1]) for co in objects]

    if fuse:
        fuseobj = doc.addObject("Part::MultiFuse","Fusion")
        fuseobj.Shapes = fcobjects
        doc.recompute()
        exportObjects = [fuseobj]
    else:
        exportObjects = fcobjects

    doc.recompute()

    if ftype == "STEP":
        ImportGui.export(exportObjects, filename)
    elif ftype == "VRML":
        # workaround for not exporting unselected objects (v0.16)
        # http://www.freecadweb.org/tracker/view.php?id=2221
        FreeCADGui.Selection.clearSelection()
        for o in exportObjects: FreeCADGui.Selection.addSelection(o)
        # TODO: ensure the filename ends with .wrl
        FreeCADGui.export(exportObjects, filename)
    elif ftype == "FREECAD":
        # TODO: export only the exportObjects by moving to a new
        #       document or deleting other objects
        doc.saveAs(filename)
    else:
        raise Exception("Unknown export file type!")

    # restore RefineShape option
    pg.SetBool("RefineModel", usersRSOption)

def test():
    # create a cadquery object
    box = cq.Workplane("XY").box(10,10,10)

    objects = [
        (box, (1.,0.,0.)),
        (box.translate((10,0,0)), (0.,1.,0.)),
    ]

    export("STEP", objects, os.path.expanduser("~/test.step"), True)
    export("VRML", objects, os.path.expanduser("~/test.wrl"), True)
    export("FREECAD", objects, os.path.expanduser("~/test.fcstd"), False)
    export("X3D", objects, os.path.expanduser("~/test.x3d"), True)

if __name__ == "__main__":
    test()
