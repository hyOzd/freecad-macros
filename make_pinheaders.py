#!/usr/bin/python
#
# This a cadquery script to generate all pin header models in X3D format.
#
# It takes a bit long to run! It can be run from cadquery freecad
# module as well.

import cadquery as cq
from Helpers import show
import os
from export_x3d import exportX3D, Mesh

npins = 5          # number of pins
sep = 2.54         # distance between pins
chamfer = 0.4      # chamfering of plastic base
hb = 2.54          # height of plastic base
hb2 = 0.5          # height of bottom cut
pw = 0.64          # width of a pin
pw_tip = pw/2      # width of pin at the tip
hp = 11.54         # height of a pin
h_tip = pw_tip*1.5 # height of pins tip
pos_y = -3         # vertical start position of a pin

case_color = (0.1, 0.1, 0.1)
pins_color = (1, 0.941, 0)

outdir = "" # handled below

def make_pinheader(npins):
    # create the base
    # left edge
    points = [
        (-sep/2, -sep/2+chamfer),
        (-sep/2, sep/2-chamfer)    
        ]
    
    # top edges
    for i in range(npins):
        points.append((-sep/2+chamfer+sep*i, sep/2))
        points.append((sep/2-chamfer+sep*i, sep/2))
        points.append((sep/2+sep*i, sep/2-chamfer))
    
    # right edge
    points.append((sep/2+sep*(npins-1), -sep/2+chamfer))
    
    # bottom edges
    for i in reversed(range(npins)):
        points.append((sep/2-chamfer+sep*i, -sep/2))
        points.append((-sep/2+chamfer+sep*i, -sep/2))
        points.append((-sep/2+sep*i, -sep/2+chamfer))
    
    base = cq.Workplane("front").polyline(points).extrude(hb)
    # pin holes
    base = base.faces(">Z").workplane().rarray(sep, 1, npins, 1).rect(pw, pw).cutThruAll()
    # bottom cut
    base = base.faces("<X").workplane().center(0,-(hb-hb2)/2).rect(sep-2*chamfer, hb2).cutThruAll()
    # barely fillet the top
    base = base.faces(">Z").fillet(0.1)

    # make pins
    def makePin():
        pin = cq.Workplane(cq.Plane.named("front",(0,0,pos_y))). \
            rect(pw_tip, pw_tip).workplane(offset=h_tip).rect(pw,pw).workplane(offset=hp-h_tip*2). \
            rect(pw,pw).workplane(offset=h_tip).rect(pw_tip, pw_tip).loft(ruled=True)
        return pin

    pin = makePin()
    pins = pin.translate((0,0,0))
    for i in range(1,npins):
        pins = pins.union(pin.translate((sep*i,0,0)))
    return (base, pins)

def shapeToMesh(shape, color):
    mesh_data = shape.tessellate(1)
    return Mesh(points = mesh_data[0],
                faces = mesh_data[1],
                color = color)

def make_one(npins):
    """Creates a pin header model file for given number of pins."""
    print("Generating pinheader %d" % npins)
    case, pins = make_pinheader(npins)
    exportX3D([shapeToMesh(case.toFreecad(), case_color),
               shapeToMesh(pins.toFreecad(), pins_color)],
              outdir+"/pinheader_1x%d.x3d" % npins)
    print("Done pinheader %d" % npins)

def make_all():
    """Creates all straight pin headers from 1 to 40 pins in X3D format in
    parallel."""
    global outdir
    outdir = os.path.abspath("./generated_pinheaders/")
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    from multiprocessing import Pool

    p = Pool()
    p.map(make_one, list(range(1,40+1)))

# when run from command line
if __name__ == "__main__":
    make_all()

# when run from freecad-cadquery
if __name__ == "temp.module":
    case, pins = make_pinheader(5)
    
    show(case, (60,60,60,0))
    show(pins)
