#!/usr/bin/python
#
# This is a cadquery script.
#
from cadquery import *
from export_x3d import exportX3D, Mesh

outfile = "/home/heyyo/Desktop/cctest.x3d"

def shapeToMesh(shape, color):
    mesh_data = shape.tessellate(1)
    return Mesh(points = mesh_data[0],
                faces = mesh_data[1],
                color = color)

box = Workplane("XY").box(1,2,3)
shape = box.toFreecad()

exportX3D([shapeToMesh(shape, (1, 0, 0))], outfile)
