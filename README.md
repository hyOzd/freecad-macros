This repository contains Python scripts that I have created for
freecad (macros).

# export_x3d.py
Exports current scene as X3D file.

# export_all_stl.py
Exports all visible objects as separate STL files.

# make_pinheaders.py
Generates pin header models in X3D format.

# make_qfp.py
Generates QFP models in X3D format.
